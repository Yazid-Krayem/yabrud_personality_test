import React from 'react';

export default class Questionnaire extends React.Component {

  renderViewMode() {

    const {question_title, answer, toggleChangeJB, result, key} = this.props;

    console.log('result', this.props);

    return (
      <div >

        <div >
          <hr/>
          <div >
            <h1>{question_title}</h1>
            < br/> {answer.map(a => (
              <div key={a.answer_id}><input type="checkbox"/>{a.answer_text}

              </div>
            ))}

          </div>
        </div>
      </div>
    );
  }

  render() {
    return (
      <div>
        {this.renderViewMode()}
      </div>
    );
  }
}