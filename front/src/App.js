import React, {Component} from 'react';
import {Switch,Route,withRouter} from 'react-router-dom';
import './App.css';
import Questionnaire from './Components/questionnaire';
import HomePage from './Components/homePage';

class App extends Component {
  state = {
    question_list: [],
    error_message: '',
    result: [],
    user_list:[],
    isJB: false,
    isChecked:false
  };

  getQuestionnaireList = async order => {
    try {
      const response = await fetch(`http://localhost:8080/question`);
      const question = await response.json();
      if (question) {
        this.setState({question_list: question});
      } else {
        this.setState({error_message: question.message});
      }
    } catch (err) {
      this.setState({error_message: err.message});
    }
  };

  /// add user information in order to start the quiz
  quizCreate = ()=>{


      return <div className="userName">

      {this.state.user_list.map(x => (
        <HomePage
          key={x.id}
          auth0_sub={x.auth0_sub}
          addUser={this.addUser}

        />

      ))}
     </div>

  }


  /// fetching questions and answers
  questionnaire = () => {
    const inner = this.state.question_list;
    var dateArr = Object.values(inner.reduce((result, {question_id, question_title, answer_id, answer_text}) => {
      // Create new group
      if (!result[question_id])
        result[question_id] = {
          question_id,
          question_title,
          answers: []
        };

      // Append to group
      result[question_id]
        .answers
        .push({answer_id, answer_text});
      return result;
    }, {}));
    return (
      <div  >

<div >

<form onSubmit={this.onSubmit}>
 <br />
{dateArr.map(x => (
  <Questionnaire
    keys={x.question_id}
    question_id={x.question_id}
    question_title={x.question_title}
    question_type={x.question_type}
    answer={x.answers}
    toggleChangeJB={this.toggleChangeJB}
    result={this.state.result}

  />

  ))}

  <input type="submit"/>
</form>
</div>
</div>
    )}


  // for testing
  // checker = async(y) => {
  //   // var found = this.state.result.find(function(element) {   return element =
  //   // y.answer_id; });
  //   var n = this
  //   .state
  //   .result
  //   .includes(y.answer_id);
  //   var found = this.state.result.find(function(element) {   return element =
  //   y.answer_id; });

  // if (found === y.answer_id) {
  //  await this.setState({isChecked:true})
  //  console.log('s')

  // } else {
  //   this.setState({isChecked:false})
  // }

  // }






  /// onChange checkBox => add the answer's vaule in the localStorage
  toggleChangeJB = async(y, e) => {


    /// Assumption
    //  get checked to answer's vaule who already checked by refresh  with the same session

  //   var localy = localStorage.getItem('answers')
  //   var n = this
  //   .state
  //   .result
  //   .includes(y.answer_id);
  //   var found = this.state.result.find(function(element) {   return element =
  //   y.answer_id; });

  // if (found === y.answer_id) {
  //  await this.setState({isChecked:true})
  //  console.log('s')

  // } else {
  //   this.setState({isChecked:false})
  // }




    this.setState(previousState => ({
      result: [
        ...previousState.result,
        y.answer_id
      ]
    }));


    /// here the localStorage should be reset


  }




  /// onSubmit button => will provide result of answers
  onSubmit = evt => {
    evt.preventDefault();
    var radios = document.getElementsByName('answer');

    let arr = [];

    for (var i = 0, length = radios.length; i < length; i++) {
      if (radios[i].checked) {
        arr.push(radios[i].value);
      }
    }

    console.log('from submit push answer_id to a new array  ', arr);
    // function to insert the array in to the database

  }

///////////////////////////////////////////
      /// React life cycle  ////
///////////////////////////////////////////

  //check if the there is data in localStorage
  componentWillMount() {
    localStorage.getItem('answers') && this.setState({
      result: JSON.parse(localStorage.getItem('answers'))
    })
  }

  // if there is no data in dataStorage will fetch data from database
  componentDidMount() {
    if (1 === 1) {
      this.getQuestionnaireList();
    } else {
      console.log('from local storage ')
    }
  }

  // create a localStorage
  componentWillUpdate(nextProps, nextState) {
    localStorage.setItem('questions', JSON.stringify(nextState.question_list))
    localStorage.setItem('answers', JSON.stringify(nextState.result))
    // localStorage.setItem('date',Date.now())
  }


/////// Routes

renderContent() {

  return (
    <Switch>

    {/* <Route  path="/" exact  component={this.HomePage}  /> */}
    <Route  path="/"  exact component={this.questionnaire}  />

    <Route render={()=><div>not found!</div>}/>


  </Switch>
  );
}
  render() {

    return (

      <div >

{this.renderContent()}


      </div>
    );
  }
}

export default withRouter(App);
