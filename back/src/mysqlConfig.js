var mysql = require('mysql2');

//phpmyadmin config
const connection = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "root",
  database:"university_testing"
});

export default connection